﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using pruebamercado.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace pruebamercado.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClienteController : ControllerBase
    {

        private readonly AplicationDbContext _context;

        public ClienteController(AplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/<ClienteController>
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                var table = await _context.Cliente.ToListAsync();
                return Ok(table);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }
        }


        // GET api/<ClienteController>/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var table = await _context.Cliente.FindAsync(id);
                if (table == null)
                {
                    return NotFound();
                }
                return Ok(table);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);

            }
        }

        // POST api/<ClienteController>
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Cliente table)
        {
            try
            {
                _context.Add(table);
                await _context.SaveChangesAsync();
                return Ok(table);
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);
            }

        }

        // PUT api/<ClienteController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] Cliente table)
        {
            try
            {
                if (id != table.id)
                {
                    return NotFound();
                }
                _context.Update(table);
                await _context.SaveChangesAsync();
                return Ok(new { message = "Tarjeta actualizada" });
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);

            }
        }

        // DELETE api/<ClienteController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var Delate = await _context.Cliente.FindAsync(id);
                if (Delate == null)
                {
                    return NotFound();
                }
                _context.Cliente.Remove(Delate);
                await _context.SaveChangesAsync();
                return Ok(new { message = "Borrado exitoso" });
            }
            catch (Exception ex)
            {

                return BadRequest(ex.Message);

            }
        }
    }
}
