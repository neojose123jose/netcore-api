﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace pruebamercado.Models
{
    public class FacturaDetalle
    {
        [Key]
        public int id { get; set; }     

        [Required]
        public int cantidad { get; set; }

        [ForeignKey("FacturaFK")]
        public Factura Factura { get; set; }
        public int FacturaFK { get; set; }

        [ForeignKey("ProductoFK")]
        public Producto Producto { get; set; }
        public int ProductoFK { get; set; }





    }
}
