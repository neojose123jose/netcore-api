﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace pruebamercado.Models
{
    public class Producto
    {
        [Key]
        public int id { get; set; }

        [Required]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string  nombre { get; set; }

        [Required]
        public int precio { get; set; }

        [Required]
        public int cantidad { get; set; }

        //[ForeignKey("ProductoFK")]
        //public ICollection<FacturaDetalle> FacturaDetalle { get; set; }

    }
}
