﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace pruebamercado.Models
{
    public class Cliente
    {
        [Key]
        public int id { get; set; }

        [Required]
        public int cedula { get; set; }

        [Required]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string nombre { get; set; }

        [Required]
        [Column(TypeName = "VARCHAR")]
        [StringLength(50)]
        public string apellido { get; set; }

        [DataType(DataType.Date)]  
        [Required]
        public DateTime fecha_nacimiento { get; set; }

        //[ForeignKey("ClinteFK")]
        //public ICollection<Factura> Factura { get; set; }




    }
}
