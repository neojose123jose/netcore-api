﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace pruebamercado.Models
{
    public class Factura
    {
        [Key]
        public int id { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime fecha_factura { get; set; }

        //[ForeignKey("FacturaFK")]
        //public ICollection<FacturaDetalle> FacturaDetalle { get; set; }

        [ForeignKey("ClinteFK")]

        public Cliente Cliente { get; set; }

        public int ClinteFK { get; set; }


    }
}
